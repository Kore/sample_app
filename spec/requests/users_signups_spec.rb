require 'rails_helper'

RSpec.describe "UsersSignups", type: :request do

  describe "The signup information" do

    it "should not be valid" do
      get signup_path
      expect{
        post users_path, user: { name:  "",
                               email: "user@invalid",
                               password:              "foo",
                               password_confirmation: "bar" }
      }.to_not change{User.count}
      assert_template 'users/new'
    end

    it "should be valid" do
      get signup_path
      expect{
        post_via_redirect users_path, user: { name:  "Example User",
                                 email: "user@example.com",
                                 password:              "password",
                                 password_confirmation: "password" }
      }.to change{User.count}.by(1)
      assert_template 'users/show'
    end

  end

end
