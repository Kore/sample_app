require 'rails_helper'
require "support/assert_select_root.rb"

RSpec.describe "SiteLayouts", type: :request do

  describe "Home page" do
    it "should have all the links" do
      get root_path
      assert_template 'static_pages/home'
      assert_select "a[href=?]", root_path, count: 2
      assert_select "a[href=?]", help_path
      assert_select "a[href=?]", about_path
      assert_select "a[href=?]", contact_path
    end
  end

end
